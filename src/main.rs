#![warn(clippy::all)]

mod primitive;
mod mesh;
mod image;
mod error;
mod drawer;
mod renderer;
mod transform;
mod vector;
mod matrix;

use crate::error::Error;
use crate::primitive::Color;
use crate::renderer::Renderer;
use crate::mesh::Mesh;
use crate::image::Image;
use crate::vector::Vec3;

fn main() -> Result<(), Error> {
    let mut render = Renderer::new();

    let head_mesh = Mesh::from_file("african_head.obj")?;
    let head_texture = Image::from_file("african_head_diffuse.tga")?;
    let head_normal_map = Image::from_file("african_head_nm_tangent.tga")?;

    let light_vector = Vec3 { x: 2.0, y: 5.0, z: 1.0 }.normalized();
    let camera_position = Vec3 { x: 0.5, y: 0.3, z: 1.0 };

    // Animation variables
    let mut v: f32 = 0.0;
    let mut u: f32 = 0.0;
    let mut w: f32 = 0.0;
    let dynamic_camera = false;

    render.light(&light_vector);
    render.camera(
        &camera_position,
        &Vec3::ZERO,
        &Vec3 { x: 0.0, y: 1.0, z: 0.0 }
    );

    loop {
        if dynamic_camera {
            v += 0.06;
            u -= 0.1;
            w += 0.03;

            render.camera(
                &Vec3 { x: v.cos() * u.sin(), y: 1.0, z: w.sin() * 2.5 },
                &Vec3 { x: v.cos(), y: u.sin(), z: w.cos() },
                &Vec3 { x: 0.0, y: 1.0, z: 0.0 }
            );
        }

        render.refresh(&Color::BLUE);
        render.model(&head_mesh, &head_texture, &head_normal_map);
        render.display()?;
    }
}
