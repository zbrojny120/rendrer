use crate::error::Error;
use crate::primitive::{
    Color,
    Size
};

#[derive(Clone)]
pub struct WinSize {
    pub cols: i32,
    pub rows: i32
}

pub struct Drawer {
    stdout: std::io::Stdout,
    win_buf: Vec<u8>,
    win_size: WinSize,
    plane_size: Size,
    img_buf: Vec<Color>
}


impl Drawer {
    const DRAWING_BLOCK: &'static [u8] = b"\xE2\x96\x84";
    const DRAWING_SEQUENCE: &'static [u8] = b"\x1b[48;2;000;000;000m\x1b[38;2;000;000;000m";

    pub fn new() -> Self {
        let (rows, cols) =  unsafe {
            let mut ws: libc::winsize = std::mem::uninitialized();
            libc::ioctl(libc::STDOUT_FILENO, libc::TIOCGWINSZ, &mut ws);
            (i32::from(ws.ws_row), i32::from(ws.ws_col))
        };

        // rows * cols * cpair_buf + \n (except for last row)
        let mut win_buf = Vec::with_capacity(
            rows as usize * cols as usize * (Self::DRAWING_BLOCK.len() + Self::DRAWING_SEQUENCE.len()) + rows as usize - 1
        );

        for _ in 0..rows {
            for _ in 0..cols {
                win_buf.extend_from_slice(Self::DRAWING_SEQUENCE);
                win_buf.extend_from_slice(Self::DRAWING_BLOCK);
            }
        }

        let mut img_buf = Vec::with_capacity((rows * cols * 2) as usize);
        for _ in 0..img_buf.capacity() {
            img_buf.push(Color { r: 0, g: 0, b: 0});
        }
        
        Drawer {
            stdout: std::io::stdout(),
            win_buf,
            win_size: WinSize { cols, rows },
            plane_size: Size { width: cols, height: rows * 2 },
            img_buf
        }
    }

    fn set_win_color_value(&mut self, pos: usize, val: u8) {
        let z = b'0';
        let v100 = val / 100;
        let v10 = (val - v100 * 100) / 10;
        let v1 = val - v100 * 100 - v10 * 10;
        self.win_buf[pos] = v100 + z;
        self.win_buf[pos + 1] = v10 + z;
        self.win_buf[pos + 2] = v1 + z;
    }

    fn set_win_vertex(&mut self, x: i32, y: i32, color: &Color) {
        let segment = (2 * x + y % 2 + 2 * self.win_size.cols * (y / 2)) as usize;
        // 7 - length of "\x1b[38;2;"
        let pos = segment * (Self::DRAWING_SEQUENCE.len() / 2) + Self::DRAWING_BLOCK.len() * (segment / 2) + 7;
        // set color every 4 characters ("000;")
        self.set_win_color_value(pos, color.r);
        self.set_win_color_value(pos + 4, color.g);
        self.set_win_color_value(pos + 8, color.b);
    }

    #[inline(always)]
    fn vertex_ref_mut(&mut self, x: i32, y: i32) -> &mut Color {
        &mut self.img_buf[(x + y * self.plane_size.width) as usize]
    }

    #[inline(always)]
    fn vertex_ref(&self, x: i32, y: i32) -> &Color {
        &self.img_buf[(x + y * self.plane_size.width) as usize]
    }

    #[inline(always)]
    pub fn set_vertex(&mut self, x: i32, y: i32, color: &Color) {
        *self.vertex_ref_mut(x, y) = color.clone();
    }

    pub fn clear(&mut self, color: &Color) {
        for vertex in self.img_buf.iter_mut() {
            *vertex = color.clone();
        }
    }

    fn move_cursor_to_origin(&mut self) -> Result<(), Error>{
        use std::io::Write;
        self.stdout.write_all(b"\x1B[0;0H")?;

        Ok(())
    }

    pub fn display(&mut self) -> Result<(), Error> {
        for y in 0..self.plane_size.height {
            for x in 0..self.plane_size.width {
                // Avoid cloning the color by evading the borrow-checker
                let color = self.vertex_ref(x, y) as *const Color;
                unsafe {
                    self.set_win_vertex(x, y, &*color);
                }
            }
        }

        self.move_cursor_to_origin()?;

        use std::io::Write;
        self.stdout.write_all(&self.win_buf)?;
        self.stdout.flush()?;

        Ok(())
    }

    #[inline(always)]
    pub fn plane_size(&self) -> Size {
        self.plane_size.clone()
    }
}
