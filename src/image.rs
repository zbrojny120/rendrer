use crate::error::Error;
use crate::primitive::{
    Color,
    Size
};

pub struct Image {
    buffer: Vec<Color>,
    size: Size
}

impl Image {
    const TGA_HEADER_SIZE: usize = 18;
    fn parse_tga_file(file_buffer: &[u8], color_buffer: &mut Vec<Color>, size: &mut Size) -> Result<(), Error> {
        if file_buffer.len() <= Self::TGA_HEADER_SIZE {
            return Err(Error::Parse);
        }

        let id_length = file_buffer[0];

        let colormap_type = file_buffer[1];
        if colormap_type != 0 {
            return Err(Error::UnsupportedFormat);
        }

        let image_type = file_buffer[2];
        size.width = i32::from(u16::from_le((u16::from(file_buffer[13]) << 0b1000) | u16::from(file_buffer[12])));
        size.height = i32::from(u16::from_le((u16::from(file_buffer[15]) << 0b1000) | u16::from(file_buffer[14])));
        color_buffer.reserve((size.width * size.height) as usize);

        // 0 - do nothing, 1 - ignore last byte of every pixel, otherwise fail
        let alpha_depth = (file_buffer[17] as usize & 0b1111) / 8;

        // 3 BRG bytes + alpha bytes
        let step = 3 + alpha_depth;

        match image_type {
            // 2 - uncompressed true-color
            2 => {
                let start = Self::TGA_HEADER_SIZE + id_length as usize;
                let end = start + (size.width * size.height) as usize * step;
                for i in (start..end).step_by(step) {
                    // TGA uses BRGa color encoding
                    color_buffer.push(Color {
                        r: file_buffer[i + 2],
                        g: file_buffer[i + 1],
                        b: file_buffer[i]
                    });
                }
            },
            // 10 - run-length encoded true-color image
            10 => {
                // byte index
                let mut i = Self::TGA_HEADER_SIZE + id_length as usize;
                // number of pixels read
                let mut n = 0usize;

                while n < (size.width * size.height) as usize {
                    let encoding_type = (file_buffer[i] & 0b1000_0000) >> 7;
                    let encoding_length = (file_buffer[i] & 0b0111_1111) + 1;
                    i += 1;

                    // following pixels are not compressed
                    if encoding_type == 0 {
                        for j in (0..(step * encoding_length as usize)).step_by(step) {
                            color_buffer.push(Color {
                                r: file_buffer[i + j + 2],
                                g: file_buffer[i + j + 1],
                                b: file_buffer[i + j]
                            });
                        }

                        n += encoding_length as usize;
                        i += encoding_length as usize * step;
                    }
                    // following pixels are compressed
                    else {
                        for _ in 0..encoding_length {
                            color_buffer.push(Color {
                                r: file_buffer[i + 2],
                                g: file_buffer[i + 1],
                                b: file_buffer[i]
                            });
                        }

                        n += encoding_length as usize;
                        i += step;
                    }
                }
            },
            _ => return Err(Error::UnsupportedFormat)
        }


        Ok(())
    }

    pub fn from_file<P: AsRef<std::path::Path>>(path: P) -> Result<Self, Error> {
        let mut file = std::fs::File::open(path)?;
        let mut file_buffer = Vec::<u8>::new();

        use std::io::Read;
        file.read_to_end(&mut file_buffer)?;

        let mut image_size = Size { width: 0, height: 0 };
        let mut color_buffer =  Vec::<Color>::new();

        Self::parse_tga_file(&file_buffer, &mut color_buffer, &mut image_size)?;

        Ok(Image {
            buffer: color_buffer,
            size: image_size
        })
    }

    pub fn size(&self) -> &Size {
        &self.size
    }

    pub fn at(&self, x: usize, y: usize) -> &Color {
        &self.buffer[x + y * self.size.width as usize]
    }
}
